require 'sinatra'
require 'sinatra/config_file'

class MyApplication < Sinatra::Application
  config_file 'config/config.yml'
  $db_config = settings.db

  require_relative 'models/init'
  require_relative 'authentication/init'
  require_relative 'routes/init'
end
