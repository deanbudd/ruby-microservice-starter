require 'rubygems'
require 'sinatra'
require 'rack/test'
require 'test/unit'

require File.expand_path '../../app.rb', __FILE__

class HealthTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app()
    MyApplication
  end

  def test_health
    get '/health?client_id=123'

    assert last_response.ok?
    assert_equal '{"status":"OK"}', last_response.body
  end
end