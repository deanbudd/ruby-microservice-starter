Sequel.migration do
  change do
    create_table(:healths) do
      String :status, :null => false
    end

    DB[:healths].insert(:status => 'OK')
  end
end
