### Setup

`docker run --name my-postgres -p5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres`

### Data

`bundle exec sequel -m db/migrations postgresql://postgres:mysecretpassword@localhost:5432/postgres`

### Query Database

`docker run -it --rm --link my-postgres:postgres postgres psql -h postgres -U postgres`

### Startup

`rackup`

### Test

`ruby test/health_test.rb`

### TODO

proper configuration. without using globals e.g using the Sinatra ConfigFile extension to load config then passes down through different components/layers

borken test. firing up the sinatra application in rack test doesn't seem to trigger Sinatra into loading the config file, therefore the database doesn't get configured



