class MyApplication < Sinatra::Application
  get '/health' do
    content_type :json
    Health.all.first.to_api.to_json
  end
end