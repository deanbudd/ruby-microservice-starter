require 'sequel'

DB = Sequel.postgres $db_config['name'], user:$db_config['user'], password:$db_config['pass'], host:$db_config['host']

# DB = Sequel.postgres 'postgres', user:'postgres', password:'mysecretpassword', host:'localhost'

require_relative 'health'
