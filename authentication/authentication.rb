module Sinatra
  module Authentication
    def authenticate!
      @current_client = request['client_id']

      halt 401, {message: 'unauthorized'}.to_json unless authenticated?
    end

    def current_client
      @current_client
    end

    def authenticated?
      !current_client.nil?
    end
  end

  helpers Authentication
end
